

Boilerplate de Frameworks html5	{#welcome}
=====================


In this repo I will Add boilerplate to use with the most popular Frameworks in the web. Some of them are Foundation 5, Bootstrap, GroundworkCSS, and others.

Framework List
----------

 1. Foundation 5 from http://foundation.zurb.com
 2. Bootstrap
 3. groundworkCSS
 4. Tuk Tuk
 5. Lungo


Comments
---------

**SEO** It will contain most popular SEO tags, some of them you have to use careful like "canonical rel". 

> **NOTE:**
> 
> - Meta tag "description". This could be the same that og_description
> - Meta tag "keywords". Try to use only between 5 to 8.


#### <i class="icon-file"></i> Favicons and touch icons

The code for favicons and touch icons it's generate for http://realfavicongenerator.net/

#### <i class="icon-folder-open"></i> CSS Place

Some notes about CSS
> - Try to use CSS to Develop and then
> - Minify your CSS to Production 
> - and if you could, concat your CSS

I will put package.json and gulp file to to this 

#### <i class="icon-pencil"></i> JS place

The same notes for JS
> - Try to use JS to Develop and then
> - Minify your JS to Production 
> - and if you could, concat your JS



----------


Instructions to use
---------------

**Clone or download the zip** 

> **NOTE:**
> 
> - Just download or clone de project
> - Enjoy the boilerplate




#### <i class="icon-download"></i> How to use gulp

Install nodejs, http://nodejs.org/ 
Install Gulp https://github.com/gulpjs/gulp/blob/master/docs/getting-started.md
Then just put

    npm install
    
You can see the task in the gulpfile.js

#### <i class="icon-upload"></i> Explanation of Gulp tasks



> **NOTE:** I will write this section later. Meanwhile yo u can read the gulpfile.js it has comments.

Any suggestion or comments they'll be welcome :D